const baseUrl = "https://apj.xyhelp.cn";
// const baseUrl = "http://192.168.2.105:8888";


function handleRes(res,resolve,reject){
  if(!res.data.needLogin && res.data.success){
    resolve(res)
  }else if(res.data.needLogin){
    toLogin().then(()=>{
      wx.hideToast();
    })
  }else{
    reject(res);
    // console.log('错误信息为',res.data.errorMsg);
  }
    
  }


const http = function(config = {}){
  return new Promise((resolve,reject)=>{
    wx.request({
      url: baseUrl +　config.url,
      data: config.data,
      header: config.header,
      method: config.method,
      timeout: 10000,
      success:res=>{
        if(res.statusCode === 200){
          handleRes(res,resolve,reject);
        }else{
          console.log(res);
          reject(res)
        }
      },
      fail:res=>{
        reject(res)
      }
    })
  })
}


function toLogin(){
  wx.showToast({
    title: '登录中',
    icon:'loading',
    duration:5000
  })
  return new Promise((resolve,reject)=>{
    wx.login({
    success: res => {
      if(res.code){
        // 获取并存储token
        // console.log('res-code',res.code);
        getToken({code:res.code}).then(res=>{
          // console.log('getToken-res',res);
          wx.setStorageSync('token', res.data.data.token);
          resolve();
        }).catch(res=>{
          // console.log('获取token失败',res);
          reject();
        })
      }
    },
    fail:res=>{
      reject(res);
    }
  })
  })

}

 
function getGoods(params){
  return http({
    url:`/goods?current=${params}&size=${300}`,
    data:null,
    method:'GET'
  })
}

// 提交订单
function postOrderForm(data){
  let token = wx.getStorageSync('token');
   return http({
    url:'/order',
    data,
    header: {
      'Authorization':token
    },
    method:'POST'
  })
}

function getToken(data){
  let code = data.code;
  return http({
    url:`/login?code=${code}`,
    data:null,
    method:'POST'
  })
}
// 客户订单查询
function getOrders(param={}){
  let token = wx.getStorageSync('token');
  return http({
    url:`/order?current=${param.current}&status=${param.status}&size=300`,
    data:null,
    header: {
      'Authorization':token
    },
    method:'GET'
  })
}

// 取消订单
function cancelOrder(data){
  let token = wx.getStorageSync('token');
  return http({
    url:`/order/${data.orderId}`,
    data:null,
    header:{
      'Authorization':token
    },
    method:'PUT'
  })
}

// 查询订单是否已经支付成功
function checkPaySuccess(param){
  let token = wx.getStorageSync('token');
  return http({
    url:`/order/${param}`,
    data:null,
    header:{
      'Authorization':token
    },
    method:'GET'
  })
}

// 重新支付
function rePay(param){
  let token = wx.getStorageSync('token');
  return http({
    url:`/order/repay/${param}`,
    data:null,
    header:{
      'Authorization':token
    },
    method:'POST'
  })
}

function payOrder(data={}){
  wx.showToast({
    title: '请求中',
    icon:'loading',
    duration:5000
  })
  return new Promise((resolve,reject)=>{
    wx.requestPayment({
      // appId:data.appId,
      nonceStr:data.nonceStr,
      package:data.packageValue,
      paySign:data.paySign,
      signType:data.signType,
      timeStamp:data.timeStamp,
      success:res=>{
        resolve();
      },
      fail:res=>{
        reject();
      }
    })
  })
}
module.exports = {
  getGoods,
  postOrderForm,
  getToken,
  getOrders,
  cancelOrder,
  toLogin,
  payOrder,
  checkPaySuccess,
  rePay
}