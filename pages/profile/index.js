// pages/profile/index.js
const API = require('../../utils/api')
const app = getApp()
Page({
  data: {
    pageTopHeight: app.globalData.pageTopHeight,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  
   onLoad:function(options){
   
   },

  onShow: function (options) {

  },
  // 点击去已支付订单页面
  toPaid(){
    let token = wx.getStorageSync('token');
    if(!token){
      API.toLogin().then(()=>{
      wx.hideToast();
      app.globalData.orderToCheck = true;
      wx.navigateTo({
      url: '/pages/order/had_pay/index',
    })
    }).catch(()=>{
      wx.hideToast();
      wx.showToast({
        title: '登录失败',
        icon:'error'
      })
    }); 
    }else{
    app.globalData.orderToCheck = true;
    wx.navigateTo({
      url: '/pages/order/had_pay/index',
    })
  }
    
  },

  // 点击去未支付订单页面
  toPrepaid(){
    let token = wx.getStorageSync('token');
    if(!token){
      API.toLogin().then(()=>{
      wx.hideToast();
      app.globalData.switchToTab = true;
     wx.navigateTo({
      url: '/pages/order/no_pay/index',
    })
    }).catch(()=>{
      wx.hideToast();
      wx.showToast({
        title: '登录失败',
        icon:'error'
      })
    }); 
    }else{
    app.globalData.switchToTab = true;
    wx.navigateTo({
      url: '/pages/order/no_pay/index',
    })
  }
  }

})