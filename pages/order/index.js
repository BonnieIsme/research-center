// pages/order/order.js
const API = require('../../utils/api');
const app = getApp();
Page({
  data: {
    pageTopHeight: app.globalData.pageTopHeight,
    list:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) { 
    this.setGoods(1);
 
  },

  onShow:function(){
   
  },
  
  onPullDownRefresh: function () {
    // 下拉刷新，清空缓存，加载第一次数据
    wx.showNavigationBarLoading();
    this.setGoods(1);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },


  // 点击“立即下单后”的处理函数
  handleOrder(e){
    let token = wx.getStorageSync('token');
    if(!token){
      // 没有token，登录请求token
      // console.log('未登录');
      API.toLogin().then(()=>{
        wx.hideToast();
        let t = wx.getStorageSync('token');
        console.log('登录后获取token',t);
        this.toCheck(e);
      }).catch(()=>{
        wx.hideToast();
        wx.showToast({
          title: '登录失败',
          icon:'error'
        })
      });
    }else{
      this.toCheck(e);
    }
  },

  // 到详情页
  toDetails(e){
    const element = JSON.stringify(e.currentTarget.dataset.element);
    wx.navigateTo({
      url: `/pages/order/details/index?element=${encodeURIComponent(element)}`,
    })
    app.globalData.switchToTab = true;
  },

  // 预约页到结账页，设置orderToCheck为true
  toCheck(e){
    const element = JSON.stringify(e.currentTarget.dataset.element);
    wx.navigateTo({
      url: `/pages/order/check/index?element=${encodeURIComponent(element)}`,
    })
    app.globalData.orderToCheck = true;
  },


  // 设置商品列表
  setGoods(index){
  const that = this;
  API.getGoods(index).then(res => {
    let array = res.data.data.records;
    array.forEach(item => {
      item.oldPrice = (parseFloat(item.oldPrice)/100).toFixed(2);
      item.newPrice = (parseFloat(item.newPrice)/100).toFixed(2);
    })
    that.setData({
      list:array
    });
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.showToast({
      title: '已刷新',
      icon:'success'
    })
  })
  }
})

