// pages/order/had_pay/index.js
const app = getApp();
const API = require('../../../utils/api')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageTopHeight: app.globalData.pageTopHeight,
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {

   this.getOrders();
  },

  getOrders(){
    API.getOrders({
      current:1,
      status:'paid'
    }).then(res=>{
      let array = res.data.data.records.map(item => {
        return {
          orderId:item.id,
          total:(item.total/100).toFixed(2),
          quantity:item.goodsNumber,
          linkman:item.linkman,
          tel:item.tel,
          createTime:item.createTime.replace('T'," "),
          paidTime:item.paidTime.replace('T'," "),
          detail:{
            title:item.goods.title,
            image:item.goods.image,
            // introduction:item.goods.introduction,
          },
        }
      })
      this.setData({
        list:array
      })
    })
  },

  toDetails(e){
    const element = JSON.stringify(e.currentTarget.dataset.element);
    wx.navigateTo({
      url: `/pages/order/had_pay/paid_details/index?element=${encodeURIComponent(element)}`,
    });
    app.globalData.switchToTab = false;
  },
})