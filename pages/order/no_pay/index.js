// pages/order/no_pay/index.js
const app = getApp();
const API = require('../../../utils/api')
Page({
  data: {
    pageTopHeight: app.globalData.pageTopHeight,
    list:[
    {
      orderId:72,
      detail:  {
        image:"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3101694723,748884042&fm=26&gp=0.jpg",
        title:"测试1",
        instroduction:"测试11",
        newPrice:22,
        oldPrice:11,
        total:33,
        quantity:1
      }
    }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */

  onShow: function (options) {
    this.getOrders();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  toPay(e){
   let orderId = e.currentTarget.dataset.orderid;
    API.rePay(orderId).then(res=>{
      console.log('重新支付信息',res.data.data);
      let data = res.data.data;
      console.log('hhhhhh',data);
      API.payOrder(data)
      .then(()=>{
        // 检查支付成功与否
        // API.checkPaySuccess(data.orderId).then(res=>{
        //   console.log('支付情况',res.status);
        //   if(res.status === 'paid'){
        //     this.getOrders();     
        //     wx.hideToast();
        //     wx.showToast({
        //       title: '支付成功',
        //       icon:'success'
        //     })
        //   }else{
        //     wx.hideToast();
        //     wx.showToast({
        //       title: '支付失败',
        //       icon:'error'
        //     })
        //   } 
        // })
        wx.hideToast();
        wx.showToast({
          title: '支付完成',
          icon:'success'
        })
        
      }).catch(()=>{
        wx.hideToast();
            wx.showToast({
              title: '支付失败',
              icon:'error'
            })
      })
    })
  //  console.log('去支付',orderId);
  },


  getOrders(){
    API.getOrders({
      current:1,
      status:'unpaid'
    }).then(res=>{
      let array = res.data.data.records.map(item => {
        return {
          orderId:item.id,
          total:(item.total/100).toFixed(2),
          quantity:item.goodsNumber,
          detail:{
            title:item.goods.title,
            image:item.goods.image,
            introduction:item.goods.introduction,
            oldPrice:(item.goods.oldPrice/100).toFixed(2),
            newPrice:(item.goods.newPrice/100).toFixed(2)
          },
        }
      })


      this.setData({
        list:array
      })

    })
  },

  toCancel(e){
    let orderId = e.currentTarget.dataset.orderid;
    wx.showModal({
      title:'提示',
      content:'确认取消该订单？',
      success:res=>{
        if(res.confirm){
          API.cancelOrder({orderId:orderId}).then(res=>{
          console.log('yes',res);
          wx.showToast({
            title: '取消成功',
            icon: 'success',
            mask: true,
            duration:2000
          })
          this.getOrders();     
        }).catch(res=>{
          console.log('no',res);
          wx.showToast({
            title: '取消失败',
            icon: 'error',
            mask: true,
            duration:2000
          })
        })
        }else if(res.cancel){
          wx.showToast({
            title: '取消操作',
            icon: 'success',
            mask: true,
            duration:2000
          })
        }
        
      }
    })
  }
})