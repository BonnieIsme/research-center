// pages/order/check/index.js
const API = require('../../../utils/api')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageTopHeight: app.globalData.pageTopHeight,
    pathToOrder:"/pages/order/index",
    pathToDetail:"/pages/order/details/index",
    flag:app.globalData.switchToTab,
    item:{},
    name:"",
    tel:"",
    note:"",
    quantity:1,
    total:0
  },

  onLoad: function (options) {
    const element = JSON.parse(decodeURIComponent(options.element));
    this.setData({
      item:element,
      total:element.newPrice,
    })
  },

  onShow:function(){
  
  },

 

  changeQuantity(e){
    this.setData({
      quantity:e.detail.value,
      total:(parseFloat(e.detail.value * this.data.item.newPrice * 1000)/1000).toFixed(2)
    })
  },

  setName(e){
    this.setData({
      name:e.detail.value
    })
  },

  setTel(e){
    this.setData({
      tel:e.detail.value
    })
  },

  setNote(e){
    this.setData({
      note:e.detail.value
    })
  },

  // 提交预约
 formSubmit(e){
    let formData = e.detail.value;
    let goodsId = parseInt(this.data.item.id);
    let goodsNumber = this.data.quantity;
    let message = formData.note;
    let linkman = formData.name;
    let tel = formData.tel;
    let token = wx.getStorageSync('token');
    
    if((linkman !=="" && linkman !== null) && (tel !=="" && tel !== null)){
      let pattern = /^1\d{10}$/;
      if(pattern.test(tel)){
        // 表单内容检查完毕且无异常，提交表单
         API.postOrderForm({
         "goodsId":goodsId,
         "goodsNumber":goodsNumber,
          "linkman":linkman,
          "message":message,
          "tel":tel
        }).then(res=>{
          // console.log('订单提交成功---',res.data.data);
          let data = res.data.data;
          wx.showModal({
            title:'提交成功',
            content:'是否现在支付该订单',
            success:res=>{
              if(res.confirm){
                // console.log('订单编号',data.orderId);
                // 支付订单
                API.payOrder(data)
                .then(()=>{
                  // 检查支付成功与否
                  // API.checkPaySuccess(data.orderId).then(res=>{
                  //   console.log('支付情况',res.status);
                  //   if(res.status === 'paid'){
                  //     wx.hideToast();
                  //     wx.showToast({
                  //       title: '支付成功',
                  //       icon:'success'
                  //     })
                  //   }else{
                  //     wx.hideToast();
                  //     wx.showToast({
                  //       title: '支付失败',
                  //       icon:'error'
                  //     })
                  //   } 
                  // })
                  wx.hideToast();
                  wx.showToast({
                    title: '支付已完成',
                    icon:'success'
                  })

                }).catch(()=>{
                  wx.hideToast();
                      wx.showToast({
                        title: '支付失败',
                        icon:'error'
                      })
                })
              }else if(res.cancel){
                wx.showToast({
                  title: '您已取消支付',
                  icon:'success'
                })
              }
            }
          })
          
        }).catch(res=>{
            // console.log('提交失败',res);
            if(res.data.errorMsg === '用户已存在该商品未支付的订单'){
              wx.showModal({
                title:'提交失败',
                content:'该商品还有未支付的订单,请先支付',
                icon:'error',
                success:res=>{
                  if(res.confirm){
                   wx.redirectTo({
                     url: '/pages/order/no_pay/index',
                   })
                  }
                }
              })
            }else{
              wx.showToast({
                title: '提交失败',
                icon:'error'
              })
            }
              
            
        })
      
      
      }else{
       wx.showToast({
        title: '电话号码有误',
        icon:'error'
       })
        }
    }
    else{
      let msg = null;
      if(linkman == null || linkman == ""){
        msg = "姓名不能为空";
       }else{
         msg = "电话不能为空"
      }
      wx.showToast({
        title: msg,
        icon:'error'
      })
    }
  },

  
})