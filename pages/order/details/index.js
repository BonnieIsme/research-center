// pages/order/details/index.js
const app = getApp();
const API = require('../../../utils/api')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    pageTopHeight: app.globalData.pageTopHeight,
    pageIndex:1,
    flag : app.globalData.switchToTab,
    pathToOrder:"/pages/order/index",
    path:"",
    item:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let path = options.path;
    console.log(path);
    console.log(app.globalData.switchToTab);
    let element = JSON.parse(decodeURIComponent(options.element));
    this.setData({
      item:element,
      path
    })
  },

  onShow:function(){
   
  },

  toCheck(){
    const element = JSON.stringify(this.data.item);
    wx.navigateTo({
      url: `/pages/order/check/index?element=${encodeURIComponent(element)}`,
    });
    app.globalData.switchToTab = false;
  },
    // 点击“立即下单后”的处理函数
    handleOrder(){
      let token = wx.getStorageSync('token');
      if(!token){
        // console.log('未登录');
        API.toLogin().then(()=>{
          wx.hideToast();
          let t = wx.getStorageSync('token');
          // console.log('登录后获取订单token',t);
          this.toCheck();
        }).catch(()=>{
          wx.hideToast();
          wx.showToast({
            title: '登录失败',
            icon:'error'
          })
        });
      }else{
        // console.log('已登录');
        let t = wx.getStorageSync('token');
        console.log('detail-token',t);
        this.toCheck();
      }
    },
})